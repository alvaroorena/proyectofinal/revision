<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenamientos".
 *
 * @property int $codigo_entrenamiento
 * @property string|null $tipo_entrenamiento
 * @property string|null $descripcion_ejercicios
 * @property string|null $calorias
 * @property int|null $codigo_jugadores_profesionales
 *
 * @property Jugadoresprofesionales $codigoJugadoresProfesionales
 */
class Entrenamientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenamientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_jugadores_profesionales'], 'integer'],
            [['tipo_entrenamiento', 'calorias'], 'string', 'max' => 20],
            [['descripcion_ejercicios'], 'string', 'max' => 150],
            [['codigo_jugadores_profesionales'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadoresprofesionales::className(), 'targetAttribute' => ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_entrenamiento' => 'Codigo Entrenamiento',
            'tipo_entrenamiento' => 'Tipo Entrenamiento',
            'descripcion_ejercicios' => 'Descripcion Ejercicios',
            'calorias' => 'Calorias',
            'codigo_jugadores_profesionales' => 'Codigo Jugadores Profesionales',
        ];
    }

    /**
     * Gets query for [[CodigoJugadoresProfesionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadoresProfesionales()
    {
        return $this->hasOne(Jugadoresprofesionales::className(), ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']);
    }
}
