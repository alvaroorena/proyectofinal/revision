<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidosoficiales".
 *
 * @property int $codigo_partido
 * @property string|null $fecha
 * @property string|null $hora
 * @property string|null $rival
 * @property int|null $goles_propios
 * @property int|null $goles_rivales
 * @property string|null $estadio
 *
 * @property Juegan[] $juegans
 * @property Jugadoresprofesionales[] $codigoJugadoresJugadoresProfesionales
 */
class PartidosOficiales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partidosoficiales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora'], 'safe'],
            [['goles_propios', 'goles_rivales'], 'integer'],
            [['rival', 'estadio'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_partido' => 'Codigo Partido',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'rival' => 'Rival',
            'goles_propios' => 'Goles Propios',
            'goles_rivales' => 'Goles Rivales',
            'estadio' => 'Estadio',
        ];
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::className(), ['codigo_partido' => 'codigo_partido']);
    }

    /**
     * Gets query for [[CodigoJugadoresJugadoresProfesionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadoresJugadoresProfesionales()
    {
        return $this->hasMany(Jugadoresprofesionales::className(), ['codigo_jugadores_profesionales' => 'codigo_jugadores_jugadores_profesionales'])->viaTable('juegan', ['codigo_partido' => 'codigo_partido']);
    }
}
