<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reservas".
 *
 * @property int $codigo_reserva
 * @property string|null $dni_reserva
 * @property string|null $nombre
 * @property string|null $fecha_nacimiento
 * @property string|null $posicion
 * @property string|null $club_actual
 * @property int|null $goles
 * @property int|null $asistencias
 * @property int|null $goles_generados
 * @property float|null $salario_bruto
 * @property int|null $codigo_staff
 *
 * @property Staff $codigoStaff
 */
class Reservas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reservas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento'], 'safe'],
            [['goles', 'asistencias', 'goles_generados', 'codigo_staff'], 'integer'],
            [['salario_bruto'], 'number'],
            [['dni_reserva'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 100],
            [['posicion'], 'string', 'max' => 20],
            [['club_actual'], 'string', 'max' => 30],
            [['codigo_staff'], 'exist', 'skipOnError' => true, 'targetClass' => Staff::className(), 'targetAttribute' => ['codigo_staff' => 'codigo_staff']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_reserva' => 'Codigo Reserva',
            'dni_reserva' => 'Dni Reserva',
            'nombre' => 'Nombre',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'posicion' => 'Posicion',
            'club_actual' => 'Club Actual',
            'goles' => 'Goles',
            'asistencias' => 'Asistencias',
            'goles_generados' => 'Goles Generados',
            'salario_bruto' => 'Salario Bruto',
            'codigo_staff' => 'Codigo Staff',
        ];
    }

    /**
     * Gets query for [[CodigoStaff]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoStaff()
    {
        return $this->hasOne(Staff::className(), ['codigo_staff' => 'codigo_staff']);
    }
}
