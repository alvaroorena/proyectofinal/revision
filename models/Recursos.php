<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recursos".
 *
 * @property int $codigo_recurso
 * @property string|null $tipo_recurso
 * @property float|null $importe
 * @property string|null $temporada
 * @property int|null $codigo_directivo
 *
 * @property Directivos $codigoDirectivo
 */
class Recursos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recursos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['importe'], 'number'],
            [['codigo_directivo'], 'integer'],
            [['tipo_recurso'], 'string', 'max' => 40],
            [['temporada'], 'string', 'max' => 9],
            [['codigo_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['codigo_directivo' => 'codigo_directivo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_recurso' => 'Codigo Recurso',
            'tipo_recurso' => 'Tipo Recurso',
            'importe' => 'Importe',
            'temporada' => 'Temporada',
            'codigo_directivo' => 'Codigo Directivo',
        ];
    }

    /**
     * Gets query for [[CodigoDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }
}
