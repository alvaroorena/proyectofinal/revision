<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadoresnoprofesionales".
 *
 * @property int $codigo_jugadores_no_profesionales
 * @property string|null $dni_jugadores_no_profesionales
 * @property string|null $nombre
 * @property string|null $fecha_nacimiento
 * @property int|null $material_prestado
 * @property string|null $nombre_p_m_t
 * @property string|null $dni_p_m_t
 * @property string|null $numero_cuenta_p_m_t
 * @property string|null $codigo_categoria
 *
 * @property Categoriasinferiores $codigoCategoria
 */
class JugadoresNoProfesionales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadoresnoprofesionales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento'], 'safe'],
            [['material_prestado'], 'integer'],
            [['dni_jugadores_no_profesionales', 'dni_p_m_t'], 'string', 'max' => 9],
            [['nombre', 'nombre_p_m_t'], 'string', 'max' => 100],
            [['numero_cuenta_p_m_t'], 'string', 'max' => 24],
            [['codigo_categoria'], 'string', 'max' => 5],
            [['codigo_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categoriasinferiores::className(), 'targetAttribute' => ['codigo_categoria' => 'codigo_categoria']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_jugadores_no_profesionales' => 'Codigo Jugadores No Profesionales',
            'dni_jugadores_no_profesionales' => 'Dni Jugadores No Profesionales',
            'nombre' => 'Nombre',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'material_prestado' => 'Material Prestado',
            'nombre_p_m_t' => 'Nombre P M T',
            'dni_p_m_t' => 'Dni P M T',
            'numero_cuenta_p_m_t' => 'Numero Cuenta P M T',
            'codigo_categoria' => 'Codigo Categoria',
        ];
    }

    /**
     * Gets query for [[CodigoCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCategoria()
    {
        return $this->hasOne(Categoriasinferiores::className(), ['codigo_categoria' => 'codigo_categoria']);
    }
}
