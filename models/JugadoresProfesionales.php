<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadoresprofesionales".
 *
 * @property int $codigo_jugadores_profesionales
 * @property string|null $dni_jugadores_profesionales
 * @property string|null $nombre
 * @property string|null $fecha_nacimiento
 * @property string|null $posicion
 * @property string|null $club_procedencia
 * @property int|null $partidos_jugados
 * @property int|null $goles
 * @property int|null $asistencias
 * @property int|null $goles_generados
 * @property int|null $tarjetas_amarillas
 * @property int|null $tarjetas_rojas
 * @property float|null $salario_bruto
 * @property string|null $numero_cuenta
 * @property int|null $codigo_directivo
 *
 * @property Entrenamientos[] $entrenamientos
 * @property Fechas[] $fechas
 * @property Juegan[] $juegans
 * @property Partidosoficiales[] $codigoPartidos
 * @property Directivos $codigoDirectivo
 * @property Lesiones[] $lesiones
 * @property Nacionalidades[] $nacionalidades
 */
class Jugadoresprofesionales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadoresprofesionales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento'], 'safe'],
            [['partidos_jugados', 'goles', 'asistencias', 'goles_generados', 'tarjetas_amarillas', 'tarjetas_rojas', 'codigo_directivo'], 'integer'],
            [['salario_bruto'], 'number'],
            [['dni_jugadores_profesionales'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 100],
            [['posicion'], 'string', 'max' => 20],
            [['club_procedencia'], 'string', 'max' => 30],
            [['numero_cuenta'], 'string', 'max' => 24],
            [['codigo_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['codigo_directivo' => 'codigo_directivo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_jugadores_profesionales' => 'Codigo Jugadores Profesionales',
            'dni_jugadores_profesionales' => 'Dni Jugadores Profesionales',
            'nombre' => 'Nombre',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'posicion' => 'Posicion',
            'club_procedencia' => 'Club Procedencia',
            'partidos_jugados' => 'Partidos Jugados',
            'goles' => 'Goles',
            'asistencias' => 'Asistencias',
            'goles_generados' => 'Goles Generados',
            'tarjetas_amarillas' => 'Tarjetas Amarillas',
            'tarjetas_rojas' => 'Tarjetas Rojas',
            'salario_bruto' => 'Salario Bruto',
            'numero_cuenta' => 'Numero Cuenta',
            'codigo_directivo' => 'Codigo Directivo',
        ];
    }

    /**
     * Gets query for [[Entrenamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenamientos()
    {
        return $this->hasMany(Entrenamientos::className(), ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']);
    }

    /**
     * Gets query for [[Fechas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFechas()
    {
        return $this->hasMany(Fechas::className(), ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']);
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::className(), ['codigo_jugadores_jugadores_profesionales' => 'codigo_jugadores_profesionales']);
    }

    /**
     * Gets query for [[CodigoPartidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartidos()
    {
        return $this->hasMany(Partidosoficiales::className(), ['codigo_partido' => 'codigo_partido'])->viaTable('juegan', ['codigo_jugadores_jugadores_profesionales' => 'codigo_jugadores_profesionales']);
    }

    /**
     * Gets query for [[CodigoDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Lesiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLesiones()
    {
        return $this->hasMany(Lesiones::className(), ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']);
    }

    /**
     * Gets query for [[Nacionalidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNacionalidades()
    {
        return $this->hasMany(Nacionalidades::className(), ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']);
    }
}
