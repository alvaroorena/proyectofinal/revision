<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directivos".
 *
 * @property int $codigo_directivo
 * @property string|null $dni_directivo
 * @property string|null $nombre
 * @property string|null $telefono
 * @property string|null $cargo
 * @property float|null $salario_bruto
 * @property string|null $numero_cuenta
 *
 * @property Empleados[] $empleados
 * @property Entrenadores[] $entrenadores
 * @property Gastos[] $gastos
 * @property Ingresos[] $ingresos
 * @property Jugadoresprofesionales[] $jugadoresprofesionales
 * @property Recursos[] $recursos
 * @property Staff[] $staff
 */
class Directivos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directivos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['salario_bruto'], 'number'],
            [['dni_directivo'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 100],
            [['telefono'], 'string', 'max' => 12],
            [['cargo'], 'string', 'max' => 20],
            [['numero_cuenta'], 'string', 'max' => 24],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_directivo' => 'Codigo Directivo',
            'dni_directivo' => 'Dni Directivo',
            'nombre' => 'Nombre',
            'telefono' => 'Telefono',
            'cargo' => 'Cargo',
            'salario_bruto' => 'Salario Bruto',
            'numero_cuenta' => 'Numero Cuenta',
        ];
    }

    /**
     * Gets query for [[Empleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleados()
    {
        return $this->hasMany(Empleados::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Gastos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGastos()
    {
        return $this->hasMany(Gastos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Ingresos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngresos()
    {
        return $this->hasMany(Ingresos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Jugadoresprofesionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadoresprofesionales()
    {
        return $this->hasMany(Jugadoresprofesionales::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Recursos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecursos()
    {
        return $this->hasMany(Recursos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Staff]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasMany(Staff::className(), ['codigo_directivo' => 'codigo_directivo']);
    }
}
