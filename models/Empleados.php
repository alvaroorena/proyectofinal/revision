<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados".
 *
 * @property int $codigo_empleado
 * @property string|null $dni_empleado
 * @property string|null $nombre
 * @property string|null $puesto
 * @property float|null $salario_bruto
 * @property string|null $numero_cuenta
 * @property int|null $codigo_directivo
 *
 * @property Directivos $codigoDirectivo
 */
class Empleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['salario_bruto'], 'number'],
            [['codigo_directivo'], 'integer'],
            [['dni_empleado'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 100],
            [['puesto'], 'string', 'max' => 20],
            [['numero_cuenta'], 'string', 'max' => 24],
            [['codigo_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['codigo_directivo' => 'codigo_directivo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_empleado' => 'Codigo Empleado',
            'dni_empleado' => 'Dni Empleado',
            'nombre' => 'Nombre',
            'puesto' => 'Puesto',
            'salario_bruto' => 'Salario Bruto',
            'numero_cuenta' => 'Numero Cuenta',
            'codigo_directivo' => 'Codigo Directivo',
        ];
    }

    /**
     * Gets query for [[CodigoDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }
}
