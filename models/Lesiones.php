<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lesiones".
 *
 * @property int $codigo_lesion
 * @property string|null $nombre
 * @property string|null $tipo_lesion
 * @property string|null $descripcion_lesion
 * @property string|null $plan_recuperacion
 * @property string|null $fecha_alta
 * @property string|null $fecha_baja
 * @property string|null $tiempo_estimado_recuperacion
 * @property int|null $codigo_jugadores_profesionales
 *
 * @property Jugadoresprofesionales $codigoJugadoresProfesionales
 */
class Lesiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_alta', 'fecha_baja'], 'safe'],
            [['codigo_jugadores_profesionales'], 'integer'],
            [['nombre', 'descripcion_lesion', 'plan_recuperacion'], 'string', 'max' => 100],
            [['tipo_lesion', 'tiempo_estimado_recuperacion'], 'string', 'max' => 30],
            [['codigo_jugadores_profesionales'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadoresprofesionales::className(), 'targetAttribute' => ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_lesion' => 'Codigo Lesion',
            'nombre' => 'Nombre',
            'tipo_lesion' => 'Tipo Lesion',
            'descripcion_lesion' => 'Descripcion Lesion',
            'plan_recuperacion' => 'Plan Recuperacion',
            'fecha_alta' => 'Fecha Alta',
            'fecha_baja' => 'Fecha Baja',
            'tiempo_estimado_recuperacion' => 'Tiempo Estimado Recuperacion',
            'codigo_jugadores_profesionales' => 'Codigo Jugadores Profesionales',
        ];
    }

    /**
     * Gets query for [[CodigoJugadoresProfesionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadoresProfesionales()
    {
        return $this->hasOne(Jugadoresprofesionales::className(), ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']);
    }
}
