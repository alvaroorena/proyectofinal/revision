<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Directivos */

$this->title = 'Update Directivos: ' . $model->codigo_directivo;
$this->params['breadcrumbs'][] = ['label' => 'Directivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_directivo, 'url' => ['view', 'id' => $model->codigo_directivo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="directivos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
