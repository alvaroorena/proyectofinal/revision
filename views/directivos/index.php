<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Directivos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directivos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Directivos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_directivo',
            'dni_directivo',
            'nombre',
            'telefono',
            'cargo',
            //'salario_bruto',
            //'numero_cuenta',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
