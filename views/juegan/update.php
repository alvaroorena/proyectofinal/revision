<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */

$this->title = 'Update Juegan: ' . $model->id_juegan;
$this->params['breadcrumbs'][] = ['label' => 'Juegans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_juegan, 'url' => ['view', 'id' => $model->id_juegan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="juegan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
