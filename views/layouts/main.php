<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap ">
    <?php
    NavBar::begin([
//        'brandLabel' => Yii::$app->name,
        'brandLabel' => '<img src="'.Yii::$app->getUrlManager()->getBaseUrl().'/img/banner.png">',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse my-navbar navbar-fixed-top ',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => ' navbar-nav navbar-right'],
        'items' => [
//            ['label' => 'Home', 'url' => ['/site/index']],
//            ['label' => 'About', 'url' => ['/site/about']],
//            ['label' => 'Contact', 'url' => ['/site/contact']],
//            ['label' => 'Categorías Inferiores', 'url' => ['/categoriasinferiores/index']],
//            ['label' => 'Directivos', 'url' => ['/directivos/index']],
//            ['label' => 'Empleados', 'url' => ['/empleados/index']],
//            ['label' => 'Entrenadores', 'url' => ['/entrenadores/index']],
//            ['label' => 'Fechas', 'url' => ['/fechas/index']],
//            ['label' => 'Gastos', 'url' => ['/gastos/index']],
//            ['label' => 'Ingresos', 'url' => ['/ingresos/index']],
//            ['label' => 'Recursos', 'url' => ['/recursos/index']],
//            ['label' => 'Juegan', 'url' => ['/juegan/index']],
//            ['label' => 'Jugadores no profesionales', 'url' => ['/jugadoresnoprofesionales/index']],
//            ['label' => 'Jugadores profesionales', 'url' => ['/jugadoresprofesionales/index']],
//            ['label' => 'Lesiones', 'url' => ['/lesiones/index']],
//            ['label' => 'Nacionalidades', 'url' => ['/nacionalidades/index']],
//            ['label' => 'Partidos oficiales', 'url' => ['/partidosoficiales/index']],
//            ['label' => 'Reservas', 'url' => ['/reservas/index']],
//            ['label' => 'Staff', 'url' => ['/staff/index']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <?= Html::img('@web/img/umbro.png');?>
<!--        <img src="../web/img/umbro.png"/>-->
        <?= Html::img('@web/img/jose.png');?>
<!--        <img src="../web/img/jose.png"/>-->
<!--        <p class="pull-left">&copy; Fútbol Club Soto de la Marina <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
