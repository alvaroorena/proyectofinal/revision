<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenamientos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entrenamientos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipo_entrenamiento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion_ejercicios')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'calorias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_jugadores_profesionales')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
