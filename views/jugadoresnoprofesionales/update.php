<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JugadoresNoProfesionales */

$this->title = 'Update Jugadores No Profesionales: ' . $model->codigo_jugadores_no_profesionales;
$this->params['breadcrumbs'][] = ['label' => 'Jugadores No Profesionales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_jugadores_no_profesionales, 'url' => ['view', 'id' => $model->codigo_jugadores_no_profesionales]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jugadores-no-profesionales-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
