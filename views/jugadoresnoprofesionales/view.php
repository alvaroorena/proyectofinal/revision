<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\JugadoresNoProfesionales */

$this->title = $model->codigo_jugadores_no_profesionales;
$this->params['breadcrumbs'][] = ['label' => 'Jugadores No Profesionales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="jugadores-no-profesionales-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_jugadores_no_profesionales], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->codigo_jugadores_no_profesionales], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_jugadores_no_profesionales',
            'dni_jugadores_no_profesionales',
            'nombre',
            'fecha_nacimiento',
            'material_prestado',
            'nombre_p_m_t',
            'dni_p_m_t',
            'numero_cuenta_p_m_t',
            'codigo_categoria',
        ],
    ]) ?>

</div>
