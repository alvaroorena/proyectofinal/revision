<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jugadores No Profesionales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jugadores-no-profesionales-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Jugadores No Profesionales', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_jugadores_no_profesionales',
            'dni_jugadores_no_profesionales',
            'nombre',
            'fecha_nacimiento',
            'material_prestado',
            //'nombre_p_m_t',
            //'dni_p_m_t',
            //'numero_cuenta_p_m_t',
            //'codigo_categoria',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
