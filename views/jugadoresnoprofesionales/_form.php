<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JugadoresNoProfesionales */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jugadores-no-profesionales-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni_jugadores_no_profesionales')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_nacimiento')->textInput() ?>

    <?= $form->field($model, 'material_prestado')->textInput() ?>

    <?= $form->field($model, 'nombre_p_m_t')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dni_p_m_t')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numero_cuenta_p_m_t')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_categoria')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
