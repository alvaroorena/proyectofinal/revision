<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PartidosOficiales */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partidos-oficiales-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'hora')->textInput() ?>

    <?= $form->field($model, 'rival')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goles_propios')->textInput() ?>

    <?= $form->field($model, 'goles_rivales')->textInput() ?>

    <?= $form->field($model, 'estadio')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
