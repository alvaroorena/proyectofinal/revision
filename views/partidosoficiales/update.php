<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PartidosOficiales */

$this->title = 'Update Partidos Oficiales: ' . $model->codigo_partido;
$this->params['breadcrumbs'][] = ['label' => 'Partidos Oficiales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_partido, 'url' => ['view', 'id' => $model->codigo_partido]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="partidos-oficiales-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
