<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Partidos Oficiales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partidos-oficiales-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Partidos Oficiales', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_partido',
            'fecha',
            'hora',
            'rival',
            'goles_propios',
            //'goles_rivales',
            //'estadio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
