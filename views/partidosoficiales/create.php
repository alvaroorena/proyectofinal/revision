<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PartidosOficiales */

$this->title = 'Create Partidos Oficiales';
$this->params['breadcrumbs'][] = ['label' => 'Partidos Oficiales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partidos-oficiales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
