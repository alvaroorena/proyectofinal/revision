<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lesiones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesiones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Lesiones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_lesion',
            'nombre',
            'tipo_lesion',
            'descripcion_lesion',
            'plan_recuperacion',
            //'fecha_alta',
            //'fecha_baja',
            //'tiempo_estimado_recuperacion',
            //'codigo_jugadores_profesionales',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
