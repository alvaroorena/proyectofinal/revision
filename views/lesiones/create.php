<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lesiones */

$this->title = 'Create Lesiones';
$this->params['breadcrumbs'][] = ['label' => 'Lesiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesiones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
