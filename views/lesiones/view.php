<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Lesiones */

$this->title = $model->codigo_lesion;
$this->params['breadcrumbs'][] = ['label' => 'Lesiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="lesiones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_lesion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->codigo_lesion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_lesion',
            'nombre',
            'tipo_lesion',
            'descripcion_lesion',
            'plan_recuperacion',
            'fecha_alta',
            'fecha_baja',
            'tiempo_estimado_recuperacion',
            'codigo_jugadores_profesionales',
        ],
    ]) ?>

</div>
