<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lesiones */

$this->title = 'Update Lesiones: ' . $model->codigo_lesion;
$this->params['breadcrumbs'][] = ['label' => 'Lesiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_lesion, 'url' => ['view', 'id' => $model->codigo_lesion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lesiones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
