<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gastos */

$this->title = 'Update Gastos: ' . $model->codigo_gasto;
$this->params['breadcrumbs'][] = ['label' => 'Gastos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_gasto, 'url' => ['view', 'id' => $model->codigo_gasto]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gastos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
