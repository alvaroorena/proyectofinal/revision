<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JugadoresProfesionales */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jugadores-profesionales-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni_jugadores_profesionales')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_nacimiento')->textInput() ?>

    <?= $form->field($model, 'posicion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'club_procedencia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'partidos_jugados')->textInput() ?>

    <?= $form->field($model, 'goles')->textInput() ?>

    <?= $form->field($model, 'asistencias')->textInput() ?>

    <?= $form->field($model, 'goles_generados')->textInput() ?>

    <?= $form->field($model, 'tarjetas_amarillas')->textInput() ?>

    <?= $form->field($model, 'tarjetas_rojas')->textInput() ?>

    <?= $form->field($model, 'salario_bruto')->textInput() ?>

    <?= $form->field($model, 'numero_cuenta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_directivo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
