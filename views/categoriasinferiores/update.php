<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoriasInferiores */

$this->title = 'Update Categorias Inferiores: ' . $model->codigo_categoria;
$this->params['breadcrumbs'][] = ['label' => 'Categorias Inferiores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_categoria, 'url' => ['view', 'id' => $model->codigo_categoria]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="categorias-inferiores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
