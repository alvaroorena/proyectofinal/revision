<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entrenadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni_entrenadores')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'licencia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categoría')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'teléfono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_directivo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
