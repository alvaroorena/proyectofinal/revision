<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recursos */

$this->title = 'Update Recursos: ' . $model->codigo_recurso;
$this->params['breadcrumbs'][] = ['label' => 'Recursos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_recurso, 'url' => ['view', 'id' => $model->codigo_recurso]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recursos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
