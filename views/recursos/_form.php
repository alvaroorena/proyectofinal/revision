<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Recursos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recursos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipo_recurso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'importe')->textInput() ?>

    <?= $form->field($model, 'temporada')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_directivo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
