<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nacionalidades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nacionalidades-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Nacionalidades', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_nacionalidades',
            'nacionalidades',
            'codigo_jugadores_profesionales',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
