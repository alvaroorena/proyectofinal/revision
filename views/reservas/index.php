<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reservas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Reservas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_reserva',
            'dni_reserva',
            'nombre',
            'fecha_nacimiento',
            'posicion',
            //'club_actual',
            //'goles',
            //'asistencias',
            //'goles_generados',
            //'salario_bruto',
            //'codigo_staff',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
