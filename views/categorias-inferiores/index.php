<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categorias Inferiores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categorias-inferiores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Categorias Inferiores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_categoria',
            'nombre',
            'rango',
            'numero_jugadores',
            'proximo_partido',
            //'codigo_entrenador',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
