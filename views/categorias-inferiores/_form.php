<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CategoriasInferiores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categorias-inferiores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_categoria')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rango')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numero_jugadores')->textInput() ?>

    <?= $form->field($model, 'proximo_partido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_entrenador')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
